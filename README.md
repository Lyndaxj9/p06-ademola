Omowumi L. Ademola

CS441 Mobile Game Design

Program 06

/===============================================/

Basic game in which player uses skills on a cooldown to
attack an enemy character.

Uses skeleton code from Program 05

/==============================================/

commit 056e69eda6a4e97fffbc32e0264683e6f7ebc54a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Apr 17 23:43:10 2017 -0400

    cooldown rebalance & uicolor change

commit 697cca2b9f27dbf1075fdb8968d10788dc6a8ff9
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Apr 17 23:38:03 2017 -0400

    cooldowns somewhat balanced

commit 592c78c69dcc34fd2411cf33105e18e4dd2dbc3a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Apr 17 20:57:40 2017 -0400

    started working on spell cooldowns

commit d67b474442d4a6b1c265fe570d794dc8a5534814
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Apr 17 17:36:22 2017 -0400

    game over message

commit a108ef128c74b3084f24d4ac11bc7930c99d7cc1
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Apr 17 16:42:43 2017 -0400

    fixed player defeat animation

commit 37a80b6abd724def8725d6bdf97dad30820d9d42
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Apr 16 23:46:39 2017 -0400

    linking game over logic to animations

commit 4da6e1dfbc9624b3841737fed94a6682480d8d8a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Apr 16 22:25:01 2017 -0400

    added more animations and started game over features

commit 57e5015b0fc80791a46c6cf5d69691e957ba2e7e
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Apr 15 23:00:41 2017 -0400

    added attacked/damaged animations

commit 2eabe52f53fd460b393baf1401b979ded3ae821d
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Apr 15 21:28:12 2017 -0400

    added attack animations for player

commit 678f015192502b659582f2a5ca7009727a98861f
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Apr 14 23:56:43 2017 -0400

    started adding attack animations

commit 28f15f3eb2b62377386ee475b0857d4eff725c6d
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Apr 14 23:14:03 2017 -0400

    added sprites

commit 3819ce5f148565b28a9b7a143174f361d309689d
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Apr 13 23:55:55 2017 -0400

    updated attack button labels

commit dab5bdc0bfd1c26bbafc50c18acec337a23ff510
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Apr 13 23:47:35 2017 -0400

    created nodes for scenes

commit 9a81ec136e287d88741aaa8d67ec3ce0ead3d2d7
Author: Lynda <Lynda@MacUserMacBook6.home>
Date:   Sun Apr 9 23:00:08 2017 -0400

    added scene transitions

commit 54ff315bfc1d2a900afe140c72edea22e314564b
Author: Lynda <Lynda@MacUserMacBook6.home>
Date:   Thu Apr 6 22:20:15 2017 -0400

    create initial scene

commit 583507b16bc6efa8bcc04f65e26269609ca8c95a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Wed Apr 5 01:54:21 2017 -0400

    Initial commit with README
